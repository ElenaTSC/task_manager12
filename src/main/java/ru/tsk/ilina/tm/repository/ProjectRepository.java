package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.IProjectRepository;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project removeByID(final String id) {
        final Project project = findByID(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project findByID(final String id) {
        for (Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project findByName(final String name) {
        for (Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project startByID(String id) {
        final Project project = findByID(id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project changeStatusByID(String id, Status status) {
        final Project project = findByID(id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        final Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project finishByID(String id) {
        final Project project = findByID(id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public void clear() {
        projects.clear();

    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

}
