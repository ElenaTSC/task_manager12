package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project removeByID(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

    Project findByID(String id);

    Project findByIndex(Integer index);

    Project findByName(String name);

    Project startByID(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishByID(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);


    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

    Project changeStatusByID(String id, Status status);

}
