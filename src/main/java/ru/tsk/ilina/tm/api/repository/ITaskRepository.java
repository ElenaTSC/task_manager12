package ru.tsk.ilina.tm.api.repository;


import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    void clear();

    Task startByID(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishByID(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    List<Task> findAll();

    Task removeByID(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

    Task findByID(String id);

    Task findByIndex(Integer index);

    Task findByName(String name);

    Task changeStatusByID(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

}
