package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project removeByID(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

    Project findByID(String id);

    Project findByIndex(Integer index);

    Project findByName(String name);

    Project startByID(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project changeStatusByID(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

    Project finishByID(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

}
